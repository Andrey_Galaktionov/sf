'use strict'

//--------------- MIXINS
let friendsMixin = {
    created: async function () {
        let response = await fetch("json/user-friends.json");
        let json = await response.json();
        this.friends = json;
    },
}

let profileMixin = {
    created: async function () {
        let response = await fetch("json/user-profile.json");
        let json = await response.json();
        this.user = json;
    },
}

let filesMixin = {
    created: async function () {
        let response = await fetch("json/user-files.json");
        let json = await response.json();
        this.files = json;
    }
}

let WallMessagesMixin = {
    created: async function () {
        let response = await fetch("json/user-wall-messages.json");
        let json = await response.json();
        this.messages = json;
    }
}


//--------------- Locale Components
let profileComp = {
    template: `<div class="user-logo">
                  <img :src="user.avatar" :title="fullName" :alt="fullName" class="w-100 h-auto">
                  <a href="?edit=true" class="user-logo-edit d-block w-100 text-center bg-primary violet">Редактировать</a>
               </div>`,
    mixins: [profileMixin],
    computed: {
        fullName: function () {
            return this.user.firstName + " " + this.user.lastName;
        }
    },
    data: function () {
        return {
            user: null,
        }
    },
}

let userFriends = {
    template: `<li class="user-profile__friends-item mr-2">
                  <a :href="'?userID=' + friend.id" class="user-profile__friends-link">
                    <div style="height: 60px;width: 60px">
                      <img class="w-100 h-100 user-image" :src="friend.avatar" :alt="fullName" :title="fullName">
                    </div>
                    <p class="text-center">{{ friend.firstName }}</p>
                  </a>
               </li>`,
    computed: {
        fullName: function () {
            return this.friend.firstName + " " + this.friend.lastName;
        }
    },
    props: {
        friend: Object,
    },
}

let userInfo = {
    template: `<div class="user-info__wrapper">
                  <h4 class="user-info__username">{{ user.firstName + " " + user.lastName }}</h4>
                  <hr style="margin: 0">
                  <div class="user-info__content">
                     <table class="w-100">
                        <tr>
                            <td>День рождения:</td>
                            <td>{{ user.birth }}</td>
                        </tr>
                        <tr>
                            <td>Почтовый ящик:</td>
                            <td>{{ user.email }}</td>
                        <tr>
                            <td>Место работы:</td>
                            <td >{{ user.job }}</td>
                        </tr>
                        </tr>
                     </table>
                     <hr>
                     <ul class="user-info__count-opportunities">
                         <li>
                            <a href="'?userID=' + user.id + '&photos=true'">
                               <span>Фотографии</span>
                               <span>{{ files.length }}</span>
                            </a>
                         </li>
                         <li>
                            <a href="'?userID=' + user.id + '&friends=true'">
                               <span>Друзья</span>
                               <span>{{ friends.length }}</span>
                            </a>
                         </li>
                         <li>
                            <a href="'?userID=' + user.id + '&video=true'">
                               <span>Видео</span>
                               <span>{{ files.length }}</span>
                            </a>
                         </li>
                     </ul>
                  </div>
               </div>`,
    mixins: [profileMixin, friendsMixin, filesMixin],
    data: function () {
        return {
            user: null,
            friends: null,
            files: null,
        }
    },
}

let userFiles = {
    template: `<div class="user-files__wrapper">
                 <h5 class="user-files__title">Мои файлы</h5>
                 <hr class="m-0">
                 <div class="user-image__wrap">
                    <ul class="user-files__wrapper-inner">
                       <li class="user-image__wrapper d-inline-block" 
                                    v-for="file in files"
                                    :key="file.id">
                         <img :src="file.url" :alt="file.title" :title="file.title" class="w-100 h-auto">
                       </li>
                    </ul>
                 </div>
               </div>`,
    mixins: [filesMixin],
    data: function () {
        return {
            files: null,
        }
    }
}


let userInput = {
    template: `<div class="user-info__messages-form_wrapper">
                  <textarea name="wall-message" class="w-100 p-2" rows="2"></textarea>
                  <input class="btn btn-primary w-100 violet" type="submit" value="Отправить" name="sendToWall">
               </div>`,
}

let userForm = {
    template: `<div class="user-wall__wrapper">
                    <h5 class="user-files__title">Ваше сообщение</h5>
                    <hr class="m-0">
                  <form class="user-info__messages-form" method="post" action="#">
                      <user-wall-input></user-wall-input>
                  </form>
               </div>`,
    components: {
        'user-wall-input': userInput,
    },
}

let wallMessages = {
    props: {
        message: Object,
    },
    template: `<div class="note m-3 p-3">
                  <a :href="'?userID=' + message.id" class="d-flex align-items-center">
                     <div class="note__user-avatar_wrapper">
                        <img class="note__user-avatar w-100 h-100" :src="message.avatar" :alt="message.fromName" :title="message.fromName">
                     </div>
                     <div class="note__user-name d-flex flex-column ml-3">
                        <span> {{ message.fromName }} </span>
                        <span> {{ message.date }} </span>
                     </div>
                  </a>
                  <div class="note__message-content pt-3 pb-3">
                     {{ message.text }}
                  </div>
               </div>`,
}


//--------------- Global Component
let user = new Vue({
    el: "#user-profile",
    mixins: [friendsMixin],
    components: {
        'user-friends-component': userFriends,
        'user-profile-component': profileComp,
    },
    data: {
        friends: null,
    },
})

let info = new Vue({
    el: "#profile-info",
    components: {
        'user-info': userInfo,
        'user-files': userFiles,
        'user-wall-form': userForm,
        'user-wall-messages': wallMessages,
    },
    mixins: [WallMessagesMixin],
    data: {
        messages: null,
    },
})