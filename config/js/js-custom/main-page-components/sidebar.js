'use strict'

let sidebarComponent = {
    props: ['propname'],
    template: `<li class="main__sidebar-li mb-3">
                  <a :href="propname.href" class="main__sidebar-link" :title="propname.title">
                     <i :class="propname.icon"></i> {{propname.title}}
                  </a>
               </li>`,
}

let main = new Vue({
    el: "#sidebar",
    data: {
        sidebar: [
            {id: 1, href: "#", icon: "fas fa-home mr-1", title: "Главная"},
            {id: 2, href: "#", icon: "fas fa-envelope mr-1", title: "Сообщения"},
            {id: 3, href: "#", icon: "fas fa-user-friends mr-1", title: "Друзья"},
            {id: 4, href: "#", icon: "fas fa-bell mr-1", title: "Новости"},
            {id: 5, href: "#", icon: "fas fa-search mr-1", title: "Поиск"}
        ],
    },
    components: {
        'main-sidebar-component': sidebarComponent,
    }
})
