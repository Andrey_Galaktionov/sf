'use strict'


// logotype component
let logotypeComponent = {
    template: `<div :class="classes.block">
                 <a :href="href" :class="classes.link">
                    <img :src="src" :title="title" :class="classes.image" :alt="title">
                 </a>
              </div>`,
    data: function () {
        return {
            href: "index.html",
            title: "Добро пожаловать на Sweet Family",
            src: "config/img/sweetfamily.jpg",
            classes: {
                block: "header__logotype-block",
                link: "header__logotype-link",
                image: "header__logotype-image",
            }
        }
    }
}


// header-search-form  component
let searchComponent = {
    props: {
        auth: Boolean,
    },
    template: `<template v-if="auth"> 
                  <form id="header__form" class="position-relative" method="get" action="#">
                      <input type="text" :class="classes.input" placeholder="Поиск">
                      <button type="submit" :class="classes.button">   
                          <i :class="classes.icon"></i>
                      </button>
                  </form>
                </template>`,
    data: function () {
        return {
            classes: {
                input: "header__form-input pl-2 p-1",
                icon: "fas fa-search violet",
                button: "header__btn-search position-absolute",
            }
        }
    }
}

//icons-block component
let iconsComponent = {
    props: {
        post: Object,
        auth: Boolean,
    },
    template: `<template v-if="auth"> 
                  <a :href="post.href" class="mr-3 header__link-icon" :title="post.title">
                     <i :class="post.class"></i>
                  </a>
               </template>`,
}

//user-profile-badge  component
let userProfileComponent = {
    props: {
        auth: Boolean,
        required: true,
        default: false,
    },
    template: `<transition>
                   <div class="dropdown" v-if="auth">
                      <a :class="classes.link" role="button" id="dropdownMenuLink" data-toggle="dropdown">
                         <span :class="classes.user_name">{{title}}</span>
                         <div :class="classes.image_wrapper">
                            <img :src="image" :alt="title" :title="title" :class="classes.image">
                         </div>
                      </a>
                      <div class="dropdown-menu dropdown-menu-right m-0" aria-labelledby="dropdownMenuLink">
                         <a v-for="item in items" :key="item.id" :class="classes.drop_item" :href="item.href">{{ item.text }}</a>
                      </div>
                   </div>
                   <div class="dropdown" v-else>
                      <a :class="classes.link" role="button" id="dropdownMenuLink" data-toggle="dropdown">
                         <span :class="classes.user_name + ' m-0'">Войти</span>
                      </a>
                      <div class="dropdown-menu dropdown-menu-right m-0" aria-labelledby="dropdownMenuLink">
                         <a v-for="item in itemsDefault" :key="item.id" :class="classes.drop_item" :href="item.href">{{ item.text }}</a>
                      </div>
                   </div>
               </transition>`,
    data: function () {
        return {
            image: "config/img/we.jpg",
            title: "Андрей",
            classes: {
                user_name: "header__profile_user-name",
                link: "dropdown-toggle btn header__profile_link d-flex align-items-center",
                image_wrapper: "header__profile-image_wrap",
                image: "header__profile_image w-100 h-100",
                drop_item: "dropdown-item text-left",
            },
            items: [
                {id: 1, text: "Новости", href: "#"},
                {id: 2, text: "Найти", href: "#"},
                {id: 3, text: "Редактировать", href: "#"},
            ],
            itemsDefault: [
                {id: 1, text: "Регистрация", href: "/?registration=true"},
                {id: 2, text: "Авторизация", href: "/?auth=true"},
            ],
        }
    }
}

let header = new Vue({
    el: "#header",
    data: {
        classes: [
            {id: 1, href: "/", class: "fas fa-home violet", title: "Home"},
            {id: 2, href: "/?news=true", class: "fas fa-bell violet", title: "Оповещения"},
            {id: 3, href: "/?messages=true", class: "fas fa-envelope violet", title: "Messages"}
        ],
        auth: false,
    },
    components: {
        'logotype-image-component': logotypeComponent,
        'search-form-component': searchComponent,
        'icons-block-component': iconsComponent,
        'user-profile-badge-component': userProfileComponent,
    },
})