'use strict'
//---------------------- MIXINS
let inputMixin = {
    created: async function () {
        let resp = await fetch("json/input-datas.json");
        let json = await resp.json();
        this.fields = json;
    }
}

let emailMixin = {
    created: async function () {
        let resp = await fetch("json/email.json");
        let json = await resp.json();
        this.emailArray = json;
        this.checkEmail();
    },
}

//---------------------- Local Components

let customInputName = {
    props: {
        input: Object,
        required: true,
    },
    template: `<div class="form-group d-flex flex-column">
                  <label :for="input.idInput" class="custom-label">{{ input.label }}</label>
                  <input :type="input.type" class="form-control" :id="input.idInput" :placeholder="input.placeholder" required>
               </div>`,
}

let customInputEmail = {
    props: {
        email: String,
        message: String,
        isCorrect: Boolean,
    },
    template: `<div class="form-group d-flex flex-column">
                  <label for="user-email" class="custom-label">Введите E-mail</label>
                  <input v-on:input="$emit('input', $event.target.value)" type="email" class="form-control" id="user-email" placeholder="Введите e-mail" required>
                  <span v-if="!this.isCorrect" class="error">{{ error }}</span>
               </div>`,
    data: function () {
        return {
            error: "E-mail зарегистрирован, пожалуйста, выберите другой",
        }
    },
}

let selectBirth = {
    props: {
        days: {type: Array, required: true,},
        months: {type: Array, required: true,},
        years: {type: Array, required: true,},
    },
    data: function () {
        return {
            selectedDay: "",
            selectedMonth: "",
            selectedYear: "",
            isCorrectDate: false,
            renderDate: "Дата не выбрана",
        }
    },
    template: `<div class="form-group mb-0">  
                  <label class="custom-label d-flex flex-column">Введите дату вашего рождения
                     <div class="select-group d-flex justify-content-around w-100">
                        <select v-model="selectedDay" class="form-control mr-2"> 
                           <option value="" disabled>Число</option>
                           <option v-for="(day,index) in days"
                                   :key="index" :value="day"> {{day}}</option>    
                        </select>  
                        <select v-model="selectedMonth" class="form-control mr-2"> 
                           <option value="" disabled>Месяц</option>
                           <option v-for="month in months"
                                   :key="month.value" :value="month.value"> {{month.title}}</option>    
                        </select> 
                        <select v-model="selectedYear" class="form-control"> 
                           <option value="" disabled>Год</option>
                           <option v-for="(year,index) in years"
                                   :key="index" :value="year"> {{year}}</option>    
                        </select>
                     </div>    
                     <span class="text-right w-100"> {{ renderDate }}</span>  
                  </label>
                </div>`,
    methods: {
        unCorrectDate: function () {
            this.renderDate = "Дата введена некорректно";
            this.selectedDay = "";
        },
        checkDateMonth: function () {
            if ((
                this.selectedMonth == 4 ||
                this.selectedMonth == 6 ||
                this.selectedMonth == 9 ||
                this.selectedMonth == 11) && this.selectedDay == 31) {
                this.unCorrectDate();
                return;
            }
            if (
                this.selectedMonth == 2 && this.selectedDay > 29 && this.selectedYear == "") {
                this.unCorrectDate();
                return;
            }
        },
        checkYear: function () {
            if (
                this.selectedYear % 4 == 0 &&
                this.selectedYear % 100 != 0 ||
                this.selectedYear % 400 == 0) {
                if (this.selectedMonth == 2 && this.selectedDay > 29) {
                    this.unCorrectDate();
                    return;
                }
            } else {
                if (this.selectedMonth == 2 && this.selectedDay > 28) {
                    this.unCorrectDate();
                    return;
                }
            }
        },
        checkCorrectDate: function () {
            if (
                this.selectedDay != "" &&
                this.selectedMonth != "" &&
                this.selectedYear != ""
            ) {
                this.renderDate = this.selectedDay + "." + this.selectedMonth + "." + this.selectedYear;
            }
        }
    },
    watch: {
        selectedDay: function () {
            this.checkCorrectDate();
            this.checkDateMonth();
            this.checkYear();
        },
        selectedMonth: function () {
            this.checkCorrectDate();
            this.checkDateMonth();
            this.checkYear();
        },
        selectedYear: function () {
            this.checkCorrectDate();
            this.checkYear();
            this.checkDateMonth();
        },
    }
}

let passwordInput = {
    props:{
        isCorrectPass: Boolean,
    },
    template: `<div class="form-group d-flex flex-column">
                  <div class="d-flex flex-column">
                     <label for="user-password" class="custom-label">Введите пароль:</label>
                     <input v-model="origin" type="password" class="form-control" id="user-password" placeholder="Введите пароль" required>
                  </div>
                  <span v-if="!isCorrectPass" class="error align-self-end">{{ message }}</span>
                  <div class="d-flex flex-column">
                     <label for="user-password-retry" class="custom-label">Введите пароль:</label>
                     <input v-model="retry" type="password" class="form-control" id="user-password-retry" placeholder="Повторите пароль" required>
                  </div>
               </div>`,
    data: function () {
        return {
            origin: '',
            retry: '',
            message: 'Пароли не совпадают'
        }
    },
}

let customForm = {
    props: {
        fields: Array,
        days: {type: Array, required: true,},
        months: {type: Array, required: true,},
        years: {type: Array, required: true,},
    },
    template: `<form action="#" method="post" enctype="multipart/form-data" class="d-flex flex-column align-items-center">
                 <custom-input-name
                       v-for="input in fields"
                       :key="input.id"
                       :input="input" 
                 ></custom-input-name>
                 <select-birth
                            :days="days"
                            :months="months"
                            :years="years"
                 ></select-birth>
                 <custom-email
                       v-model="mail"
                       v-on:input="checkEmail()"
                       :email="mail"
                       :isCorrect="isCorrectEmail">
                 </custom-email>
                 <custom-password 
                       ref="password"
                       :isCorrectPass="isCorrectPass"
                 ></custom-password>
                 <div class="form-group d-flex m-3 justify-content-center">
                    <input type="reset" value="Сброс" class="btn btn-secondary m-3">
                    <input v-on:click="validatePassword" v-if="isCorrectEmail" type="submit" value="Отправить" class="btn btn-primary m-3">
                    <input v-else type="submit" value="Отправить" class="btn btn-primary m-3" disabled>
                 </div>
               </form>`,
    mixins: [emailMixin],
    components: {
        'custom-input-name': customInputName,
        'select-birth': selectBirth,
        'custom-email': customInputEmail,
        'custom-password': passwordInput,
    },
    data: function () {
        return {
            mail: '',
            isCorrectEmail: true,
            emailArray: [],
            isCorrectPass: true,
        }
    },
    methods: {
        checkEmail: function () {
            for (let i = 0; i < this.emailArray.length; i++) {
                let current = this.emailArray[i].email;
                if (current == this.mail) {
                    this.isCorrectEmail = false;
                    return;
                }
                this.isCorrectEmail = true;
            }
        },
        validatePassword: function () {
            let pass = this.$refs.password.origin;
            let retry = this.$refs.password.retry;

            if (pass != retry) {
                this.isCorrectPass = false;
                return;
            }
        }
    }
}

//---------------------- Global Component

let main = new Vue({
    el: "#main-block",
    components: {
        'custom-form-component': customForm,
    },
    mixins: [inputMixin],
    data: {
        fields: null,
        days: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
        months: [
            {title: "январь", value: 1}, {title: "февраль", value: 2},
            {title: "март", value: 3}, {title: "апрель", value: 4},
            {title: "май", value: 5}, {title: "июнь", value: 6},
            {title: "июль", value: 7}, {title: "август", value: 8},
            {title: "сентябрь", value: 9}, {title: "октябрь", value: 10},
            {title: "ноябрь", value: 11}, {title: "декабрь", value: 12},
        ],
        years: [],
    },
    created: function () {
        let date = new Date();
        let year = date.getFullYear();
        while (this.years.length < 100) {
            this.years.push(year--);
        }
    },
})