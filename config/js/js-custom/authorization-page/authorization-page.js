'use strict'

//------------------- MIXINS

let getUsersMixin = {
    created: async function () {
        let response = await fetch("json/user-friends.json");
        let json = await response.json();
        this.users = json;
    }
}

//-------------------  Local Components

let loginComponent = {
    props:{
        message: String,
    },
    template: `<div class="form-group p-4">
               <div class="d-flex flex-column mb-4">
                  <label for="user-email" class="custom-label">Введите E-mail</label>
                  <input v-model="email" type="email" class="form-control" id="user-email" placeholder="Введите e-mail" required>
               </div>
               <div class="d-flex flex-column mb-4">
                  <label for="user-password" class="custom-label">Введите пароль</label>
                  <input v-model="password" type="password" class="form-control" id="user-password" placeholder="Введите пароль" required>
               </div>
               <span class="error"> {{ message }}</span>
           </div>`,
    data: function () {
        return {
            email: "",
            password: "",
        }
    }
}

let formAuth = {
    props:{
        message: String,
    },
    template: `<form class="d-flex flex-column align-items-center">
                   <login-password-component
                        ref="input"
                        :message="message"
                   ></login-password-component>
                   <div class="form-group d-flex m-3 justify-content-center">
                      <input type="reset" value="Сброс" class="btn btn-secondary m-3">
                      <input v-on:click="$emit('click')" type="button" value="Отправить" class="btn btn-primary m-3">
                   </div>
                   <span>{{ message }}</span>
               </form>`,
    components: {
        'login-password-component': loginComponent,
    },
}

//-------------------  Global Component

let main = new Vue({
    el: "#main-block",
    components: {
        'form-authorization': formAuth,
    },
    data: {
        users: [],
        message: '',
    },
    methods: {
        validateForm: function () {
            let email = this.$refs.form.$refs.input.email;
            let password = this.$refs.form.$refs.input.password;

            if (email !== "" && password !== "") {
                for (let i = 0; i < this.users.length; i++) {
                    let current = this.users[i];

                    if (
                        current.email == email &&
                        current.password == password) {
                        this.message = `Добро пожаловать ${current.firstName} ${current.lastName}`;
                        return;
                    }
                }
                this.message = `Неверная пара e-mail - пароль`;
            } else {
                this.message = `Поля не должны быть пустыми`;
            }
        }
    },
    mixins: [getUsersMixin],
})